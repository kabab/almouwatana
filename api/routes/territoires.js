var express = require('express');
var routers = express.Router();
var territoireCtrl = require('../controllers/territoireCtrl');
var jwt = require('express-jwt');
var config = require('../config');
routers.use(jwt({secret: config.secret}));

routers.use(function(req, res, next) {
	// TO-DO : add error message handeling
  if(!req.user) return res.send(401);
  return next()
});

routers.post('/', territoireCtrl.create);
routers.get('/', territoireCtrl.list);
routers.put('/:id', territoireCtrl.update);
routers.put('/:id/:responsable_id', territoireCtrl.addResponsable);
routers.delete('/:id/:responsable_id', territoireCtrl.removeResponsable);

module.exports = routers;
