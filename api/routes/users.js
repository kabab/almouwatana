var express = require('express');
var router = express.Router();
var adminCtrl = require('../controllers/administrateurCtrl');
var jwt = require("express-jwt");
var config = require('../config');

router.use(jwt({secret: config.secret}));

router.use(function(req, res, next) {
	// TO-DO : add error message handeling
  if(!req.user) return res.send(401);
  return next()
});

router.post('/', adminCtrl.register);
router.put('/:id', adminCtrl.update);
router.get('/', adminCtrl.list);
router.get('/:id', adminCtrl.view);
router.delete('/:id', adminCtrl.remove);

module.exports = router;
