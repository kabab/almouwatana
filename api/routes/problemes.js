var express = require('express');
var routers = express.Router();
var problemeCtrl = require('../controllers/problemeCtrl');
var config = require('../config');
var jwt = require('express-jwt');

routers.post('/', problemeCtrl.create);

routers.use(jwt({secret: config.secret}));

routers.use(function(req, res, next) {
	// TO-DO : add error message handeling
  if(!req.user) return res.send(401);
  return next()
});

routers.get('/', problemeCtrl.list);
routers.get('/:id/:etat', problemeCtrl.updateEtat);
routers.put('/:id', problemeCtrl.update);
routers.delete('/:id', problemeCtrl.remove);
routers.get('/statistics', problemeCtrl.statistics);
routers.get('/locations', problemeCtrl.locations);

module.exports = routers;
