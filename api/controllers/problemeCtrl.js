var Probleme = require('../models/Probleme').model;
var Message = require('../util/Message');
var config = require('../config');
var _v = require('validator');


module.exports.create = function(req, res) {

  var _m = new Message();
  var probleme = new Probleme();

  var cats = req.body.categorie.split(' - ');

  var pos = req.body.position.split('(')[1];
  pos = pos.substr(0, pos.length - 1).split(',')

  probleme.description = req.body.description;
  probleme.categorie = cats[0];
  probleme.subCategorie = cats[1];
  probleme.image = req.body.image;
  probleme.loc = pos;
  probleme.etat = 'Nouveau';  // Fixe, Ferme, Ouvert, Nouveau, Irreparables
  probleme.nom = req.body.name;
  probleme.email = req.body.email;
  probleme.tel = req.body.tel;

  /*

  if(!_v.isAlpha(probleme.declarant.nom)) {
    _m.error({code: 1000, msg: 'Invalide nom'});
  }

  if(!_v.isAlpha(probleme.declarant.prenom)) {
    _m.error({code: 1001, msg: 'Invalide nom'});
  }

  if(!_v.isEmail(probleme.declarant.email)) {
    _m.error({code: 1002, msg: 'Invalide email'});
  }

  if(_m.isError()) {
    return res.json(_m.getAll());
  }
  */

  probleme.save(function(err, probleme) {
    if(err)
      _m.error({code: 1003, msg: 'Server Error'});
    else
      _m.msg({code: 2000, msg: 'Probleme sauvgrade'});
    return res.json(_m.getAll());
  });
};

module.exports.updateEtat = function(req, res) {
  var id = req.params.id;
  var etat = req.params.etat;

  var _m = new Message();

  Probleme.findById(id, function(err, p) {
    if(!p) {
      _m.error({code: 1004, msg: 'Server error'});
      return res.json(_m.getAll());
    } else {
      p.etat = etat;
      p.save(function(err, np) {
        _m.msg({code: 2001, msg: 'Etat a change a '+etat});
        return res.json(_m.getAll());
      });
    }
  });

}

module.exports.list = function(req, res) {
  var _m = new Message();

  Probleme.find({}, null, {sort: {cree: -1}}, function(err, p) {
    if(err)
      _m.error({code: 1005, msg: 'Server error'});
    else
      _m.msg({code: 2002, msg: 'Problemes data'}, p);
    res.json(_m.getAll());
  });
};

module.exports.view = function(req, res) {
  var _m = new Message();

  var id = req.params.id;

  Probleme.findById(id, function(err, p) {
    if(err) {
      _m.error({code: 1006, msg: 'Server error'});
    } else {
      if(!p)
      _m.error({code: 1007, msg: 'Probleme introuvable'});
      else _m.msg({code: 2003, msg: 'Probleme data'}, p);
    }
    return res.json(_m.getAll());
  });
};

module.exports.update = function(req, res) {
  var _m = new Message();
  var id = req.params.id;
  Probleme.findById(id, function(err, probleme) {
    if(probleme) {
      probleme.etat = req.body.etat;
      probleme.nom = req.body.nom;
      probleme.email = req.body.email;
      probleme.tel = req.body.tel;
      probleme.description = req.body.description
      probleme.categorie = req.body.categorie;
      probleme.subCategorie = req.body.subCategorie;

      probleme.save();

      _m.msg({code: 2004, msg: 'Probléme enregistré'}, probleme);
    } else {
      _m.error({code: 1008, msg: 'Server error'});
    }

    return res.json(_m.getAll());
  });
}

module.exports.remove = function(req, res) {
  var _m = new Message();

  var id = req.params.id;

  Probleme.findById(id, function(err, p) {
    if(err) {
      _m.error({code: 1006, msg: 'Server error'});
    } else {
      if(!p)
        _m.error({code: 1007, msg: 'Probleme introuvable'});
      else {
        p.remove();
        _m.msg({code: 2003, msg: 'Probleme supprime'});
      }
    }
    return res.json(_m.getAll());
  });
}

module.exports.territoireProbleme = function() {

}

module.exports.statistics = function(req, res) {
  var _m = new Message();
  var group = {
    _id: {
      day : {$dayOfMonth: "$cree"},
      year: {$year: "$cree"},
      month: {$month: "$cree"}
      },
    count: {$sum: 1}
  };


  var promise1 = Probleme.aggregate({$group: group}).exec();
  var promise2 = Probleme.aggregate({$group: {_id: "$categorie", count: {$sum: 1}}}).exec();


  Promise.all([promise1, promise2]).then(function(data) {
    _m.msg({code: 2100, msg: 'statistics'}, data);
    return res.json(_m.getAll());
  }, function(data) {
    _m.msg({code: 1100, msg: 'statistics error'}, data);
    return res.json(_m.getAll());
  });
}

module.exports.locations = function(req, res) {
  var _m = new Message();
  Probleme.find().select("loc").exec(function(err, data) {
    if(err)
      _m.msg({code: 1101, msg: 'locations error'}, data);
    else
      _m.msg({code: 2101, msg: 'locations'}, data);
    return res.json(_m.getAll());
  });
}
