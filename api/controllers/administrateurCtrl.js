var jwt = require('jsonwebtoken');
var config = require('../config');
var Admin = require('../models/Administrateur').model;
var Message = require('../util/Message');
var validator = require('validator');

// 1012, 2008

exports.signin = function(req, res) {
  var email = req.body.email || '';
  var password = req.body.password || '';
  var msg = new Message();

  if (email == '') {
    msg.error({code: 1013, msg: 'Email invalide'});
  }

  if (password == '') { 
    msg.error({code: 1014, msg: 'Mot de passe invalide'});
  }

  if(msg.isError())
    return res.json(msg.getAll());

  Admin.findOne({'email': email}, function (err, admin) {
    if (err) {
      msg.error({code: 1015, msg: 'Server error'});
      return res.json(msg.getAll());
    }

    if (admin == undefined) {
      msg.error({code: 1016, msg: 'Utilisateur introuvable'});
      return res.json(msg.getAll());
    }

    admin.comparePassword(password, function(isMatch) {
      if (!isMatch) {
        msg.error({code: 1017, msg: 'Mot de passe erroné'});
        return res.json(msg.getAll());
      }

      var token = jwt.sign({id: admin._id, type: admin.type}, config.secret,
      { expiresInMinutes: config.token_expiration });

      msg.msg({code: 2009, msg: 'Connecté'}, {token: token, type: admin.type});

      return res.json(msg.getAll());
    });
  });
};

exports.logout = function(req, res) {
  var msg = new Message();
  if (req.user) {
    delete req.user;
    msg.msg({code: 2010, msg: 'Deconnecté'});
  }
  else {
    msg.error({code: 1018, msg: 'Server error'});
  }
  return res.send(msg.getAll());
}

exports.list = function(req, res) {
  var msg = new Message();
  Admin.find({type:'Responsable'}, function(err, list) {
    if(list) {
      msg.msg({msg: 'La liste des responsables', code: 2011}, list);
    } else {
      msg.error({code: 1019, msg: 'Server error'});
    }
    return res.send(msg.getAll());
  });
}

exports.register = function(req, res) {

  var password = req.body.password || '';
  var passwordConfirmation = req.body.passwordConfirmation || '';
  var nom = req.body.nom || '';
  var prenom = req.body.prenom || '';
  var email = req.body.email || '';
  var tel = req.body.tel || '';
  var type = req.body.type

  var msg = new Message();

  if(!validator.isEmail(email)) {
    msg.error({code: 1020, msg: 'Email invalide'});
  }

  if(password.length < 6) {
    msg.error({code: 1021, msg: 'Mot de passe invalide'});
  }

  if(password != passwordConfirmation) {
    msg.error({code: 1022, msg: 'Confirmation mot de passe est invalide'});
  }

  if(prenom == '') {
    msg.error({code: 1023, msg: 'Prenom invalide'});
  }

  if(nom =='' ) {
    msg.error({code: 1024, msg: 'Nom invalide'});
  }

  if(msg.isError())
    return res.json(msg.getAll());

  var admin = new Admin();

  admin.nom = nom;
  admin.prenom = prenom;
  admin.email = email;
  admin.tel = tel;
  admin.type = 'Responsable';
  admin.password = password;

  Admin.findOne({email: email}, function(err, u){
    if(u) {
      msg.error({code: 1025, msg: 'Email existe'});
      return res.json(msg.getAll());
    } else {
      admin.save(function(err) {
        if(err) {
          msg.error({code: 1026, msg: 'Server error'});
          return res.json(msg.getAll());
        }
        msg.msg({code: 2012, msg: 'Utilisateur enregistre'});
        return res.send(msg.getAll());
      });
    }
  });

};

exports.view = function(req, res) {
  var msg = new Message();
  if(!req.user) {
    msg.error({code: 1027, msg: 'Server error'});
    res.json(msg.getAll());
  }

  Admin.findById(req.user.id ,function(err, admin) {
    if(!admin) {
      msg.error({code: 1028, msg: 'Utilisateur introuvable'});
      return res.send(msg.getAll());
    }
    user.password = undefined;
    msg.msg({code: 2013, msg: 'Utilisateur'}, admin);
    res.json(msg.getAll());
  });
};


exports.update = function(req, res) {
  var msg = new Message();
  var id = req.params.id;

  Admin.findById(id ,function(err, admin) {

    var password = req.body.password || '';
    var passwordConfirmation = req.body.passwordConfirmation || '';
    var prenom = req.body.prenom || '';
    var nom = req.body.nom || '';
    var tel = req.body.tel || '';

    if(password.length != '') {
      if(password.length < 6) {
        msg.error({code: 1021, msg: 'Mot de passe invalide'});
      }

      if(password != passwordConfirmation) {
        msg.error({code: 1022, msg: 'Confirmation mot de passe est invalide'});
      }
    }

    if(prenom == '') {
      msg.error({code: 1023, msg: 'Prenom invalide'});
    }

    if(nom == '') {
      msg.error({code: 1024, msg: 'Nom invalide'});
    }

    if(msg.isError())
      return res.json(msg.getAll());

    admin.password = password;
    admin.prenom = prenom;
    admin.nom = nom;
    admin.tel = tel;

    admin.save(function(err, user) {
      var msg = new Message();
      if (err) {
        msg.error({code: 1030, msg: 'Server error'});
        return res.json(msg.getAll());
      }
      msg.msg({ code: 2014, msg: 'Utilisateur enregistré' }, user);
      return res.send(msg.getAll());
    });
  });
}

exports.remove = function(req, res) {
  var msg = new Message();
  var id = req.params.id;

  Admin.findById(id ,function(err, admin) {
    if(!admin) {
      msg.error({code: 1032, msg: 'Utilisateur introuvable'});
      return res.send(msg.getAll());
    }
    admin.remove();
    msg.msg({code: 2015, msg: 'Utilisateur supprime'});
    return res.send(msg.getAll());
  });
}
