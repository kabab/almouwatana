var Territoire = require('../models/Territoire').model;
var Message = require('../util/Message');
var config = require('../config');
var _v = require('validator');

// 2003, 1007

module.exports.create = function(req, res) {
  var _m = new Message();
  var territoire = new Territoire();
  territoire.nom = req.body.nom;
  territoire.points = req.body.points;

  territoire.save(function(err, t) {
    if(err) {
      _m.error({code: 1008, msg: 'Server error'});
    } else
      _m.msg({code: 2004, msg: 'Territoire enregistre'}, t);
    return res.json(_m.getAll());
  });
}

module.exports.list = function(req, res) {
  var _m = new Message();

  Territoire.find(function(err, t) {
    if(err) {
      _m.error({code: 1009, msg: 'Server error'});
    } else
      _m.msg({code: 2005, msg: 'Territoires'}, t);
    return res.json(_m.getAll());
  });
}

module.exports.addResponsable = function(req, res) {
  var responsable_id = req.params.responsable_id;
  var id = req.params.id;
  var _m = new Message();

  Territoire.findById(id, function(err, t) {
    if(t) {
      if(t.responsable.indexOf(responsable_id) >= 0) {
        _m.error({code: 1010, msg: 'Responsable déja ajouté'});
      } else {
        t.responsable.push(responsable_id);
        t.save();
        _m.msg({code: 2006, msg: 'Responasble ajoute'});
      }
    } else
      _m.error({code: 1010, msg: 'Server Error'});
    return res.json(_m.getAll());

  });
}

module.exports.removeResponsable = function(req, res) {
  var responsable_id = req.params.responsable_id;
  var id = req.params.id;
  var _m = new Message();

  Territoire.findById(id, function(err, t) {
    if(t) {
      t.responsable.pull(responsable_id);
      t.save();
      _m.msg({code: 2007, msg: 'Responasble supprime'});
    } else
      _m.error({code: 1011, msg: 'Server Error'});
    return res.json(_m.getAll());
  });
}

module.exports.remove = function(req, res) {
  var id = req.params.id;
  var _m = new Message();

  Territoire.findById(id, function(err, t) {
    if(t) {
      t.remove();
      _m.msg({code: 2008, msg: 'Territoire supprime'});
    } else
      _m.error({code: 1012, msg: 'Server Error'});
    return res.json(_m.getAll());
  });
}

module.exports.update = function(req, res) {
  var _m = new Message();
  var id = req.params.id;

  Territoire.findById(id, function(err, t) {
    if(t) {
      t.nom = req.body.nom || '';
      t.points = req.body.points;
      t.save();
      _m.msg({code: 2008, msg: 'Territoire modifié'});
    } else
      _m.error({code: 1012, msg: 'Server Error'});
    return res.json(_m.getAll());
  });

}
