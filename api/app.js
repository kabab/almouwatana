var express = require('express');
var app = express();
var jwt = require('express-jwt');
var bodyParser = require('body-parser'); //bodyparser + json + urlencoder
var morgan  = require('morgan'); // logger
var config = require('./config');
var server = require('http').Server(app)
var io = require('socket.io')(server);
var adminCtrl = require('./controllers/administrateurCtrl');

require('./util/mongo_database');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(morgan('dev'));

server.listen(config.api_port);

//Routes
var routes = {};

// routes.users = require('./route/users');
routes.territoires = require('./routes/territoires');
routes.problemes = require('./routes/problemes');
routes.users = require('./routes/users');

app.use( function(req, res, next) {
  res.set('Access-Control-Allow-Origin', 'http://localhost');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  if ('OPTIONS' == req.method) return res.send(200);
  next();
});


app.post('/problemes', function(req, res, next) {
  io.emit('news', {});
  next();
});

// app.use('/users', routes.users);

app.post('/users/signin', adminCtrl.signin);
app.post('/users/logout', adminCtrl.logout);
app.post('/users/register', adminCtrl.register);

app.use('/territoires', routes.territoires);
app.use('/problemes', routes.problemes);
app.use('/responsables', routes.users);

console.log('API start on %d', config.api_port);
