var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OperationSchema = new Schema({
  time: {type: Date, default: Date.now},
  type: String,
  description: String,
  probleme: Schema.Types.ObjectId,
  administrateur: Schema.TypesObjectId
});

module.exports = {
  schema: OperationSchema,
  model: mongoose.model('OperationSchema', OperationSchema)
};
