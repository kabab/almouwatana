var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorieSchema = new Schema({
  titre: String,
  subcat: [String]
});

module.exports = {
  schema: CategorieSchema,
  model: mongoose.model('Categorie', CategorieSchema)
};
