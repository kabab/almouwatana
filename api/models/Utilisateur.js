var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UtilisateurSchema = new Schema({
  nom: String,
  prenom: String,
  email: String,
  tel: String,
});

module.exports = {
  schema: UtilisateurSchema,
  model: mongoose.model('Utilisateur', UtilisateurSchema)
};
