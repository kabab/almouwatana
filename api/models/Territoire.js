var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TerritoireSchema = new Schema({
  nom: String,
  points: [],
  responsable: [Schema.Types.ObjectId]
});

module.exports = {
  schema: TerritoireSchema,
  model: mongoose.model('Territoire', TerritoireSchema)
};
