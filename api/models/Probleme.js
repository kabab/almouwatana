var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CategorieSchema = require('./Categorie').schema;
var UtilisateurSchema = require('./Utilisateur').schema;

var ProblemeSchema = new Schema({
  image: String,
  loc: { type: [Number], index: '2d' },
  etat: String,
  description: String,
  cree: {type: Date, default: Date.now},
  nom: String,
  email: String,
  tel: String,
  categorie: String,
  subCategorie: String
});

module.exports = {
  schema: ProblemeSchema,
  model: mongoose.model('Probleme', ProblemeSchema)
};
