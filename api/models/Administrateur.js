var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var config = require('../config');
var Schema = mongoose.Schema;

var AdministrateurSchema = new Schema({
  nom: String,
  prenom: String,
  tel: String,
  email: String,
  password: String,
  type: String,
  territoire: [Schema.Types.ObjectId]
});

AdministrateurSchema.pre('save', function(next) {
  var user = this;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(10, function(err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) return next(err);
        user.password = hash;
        next();
    });
  });
});

AdministrateurSchema.methods.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(isMatch);
    });
};

module.exports = {
  schema: AdministrateurSchema,
  model: mongoose.model('Administrateur', AdministrateurSchema)
};
