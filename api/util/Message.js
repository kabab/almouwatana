var Message = function() {
  var isError = false
  var playLoad = [];

  this.isError = function() {
    return isError;
  };

  this.getAll = function() {
    return playLoad;
  };

  this.error = function(ob) {
    isError = true;
    playLoad.push({error: true,
                 code: ob.code,
                 data: null,
                 msg: ob.msg});
  };
  
  this.msg = function(ob, data) {
    playLoad.push({error: false,
                   code: ob.code, 
                   data: data,
                   msg: ob.msg});
  };
};

module.exports = Message;