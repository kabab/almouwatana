// var process = require('process');
var config  = {
  local: {
    mongo_uri: 'mongodb://localhost/almouwatna',
    domaine_name: 'menutik.com',
    api_port: '3001',
    redis_port: '6379',
    secret: 'aMdoeb5ed87zorRdkD6greDML81DcnrzeSD648ferFejmplx',
    domain: 'menutik',
    url: 'http://localhost/Menutik/website',
		token_expiration: 60,
    upload_dir: '/var/www/html/Menutik/website/uploads/'
	//upload_dir: '/var/www/Menutik/website/uploads/'
  },

  test: {
    mongo_uri: 'mongodb://localhost/almouwatna',
    domaine_name: 'barhoch.bitnamiapp.com',
    api_port: '8080',
    redis_port: '6379',
    secret: 'aMdoeb5ed87zorRdkD6gHz4ML81DcnrzeSD648ferFejmplx',
    domain: 'barhoch.bitnamiapp.com',
    url: 'http://barhoch.bitnamiapp.com/',
		token_expiration: 60,
		upload_dir: '/home/bitnami/htdocs/menutik/uploads/'

  }
};

module.exports = config[process.env.NODE_ENV || 'local'];
