//Main Function
var map = null;

var Checkly = function() {

    var openNav = function () {
        var _btn = $('.btn-run-nav');
        $("#wrapper").toggleClass("toggled");
        if(!_btn.hasClass('open')){
            _btn.addClass('open');
            _btn.find('i').removeClass('fa-bars').addClass('fa-times');
        }else{
            _btn.removeClass('open');
            _btn.find('i').addClass('fa-bars').removeClass('fa-times');
        }
    };


    var setBounderies = function() {
      var minZoomLevel = 12;
      var maxZoomLevel = 21;
      var strictBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng( 33.495279, -7.7165028),
        new google.maps.LatLng( 33.6486015, -7.4582757)
      );

      google.maps.event.addListener(map, 'zoom_changed', function() {
         if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
         if (map.getZoom() > maxZoomLevel)  map.setZoom(maxZoomLevel);
       });
      // Listen for the dragend event
      google.maps.event.addListener(map, 'dragend', function() {
        if (strictBounds.contains(map.getCenter())) return;

        // We're out of bounds - Move the map back within the bounds

        var c = map.getCenter(),
            x = c.lng(),
            y = c.lat(),
            maxX = strictBounds.getNorthEast().lng(),
            maxY = strictBounds.getNorthEast().lat(),
            minX = strictBounds.getSouthWest().lng(),
            minY = strictBounds.getSouthWest().lat();

        if (x < minX) x = minX;
        if (x > maxX) x = maxX;
        if (y < minY) y = minY;
        if (y > maxY) y = maxY;

        map.setCenter(new google.maps.LatLng(y, x));
      });
    }

    var initializer =  function() {
        if(typeof(google) === 'undefined'){
            return;
        }
        var Latlng = new google.maps.LatLng(33.5731104, -7.589843399999999);
        var mapOptions = {
          center: Latlng,
          zoom: 15
        };

        if(document.getElementById('map-canvas') != null){
            map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
        }

        setBounderies();
    };

    var runButtonLoading = function () {
        $('#btn-check').on('click', function () {
            var $btn = $(this).button('loading');
            $btn.button('reset');
        });
    };

    var runDisplayActions = function () {
        $('.table > tbody > tr').on('hover', function () {
           console.log('1');
        });
    };

    var runNotice = function (msg) {
        // create the notification
        var notification = new NotificationFx({
            message : '<div class="ns-thumb"><span class="icon-notice"><i class="fa fa-bell-o fa-2x"></i></span></div><div class="ns-content"><p>'+msg+'</p></div>',
            layout : 'other',
            ttl : 5000,
            effect : 'thumbslider',
            type : 'error', // notice, warning, error or success
            onClose: function () {
                $('.ns-box').remove();
            }
        });
        // show the notification
        notification.show();
    };

    return {
        // init pages
        init: function() {
            initializer();
            runButtonLoading();
            runDisplayActions();
        },
        runNavigation : function () {
            openNav();
        },
        notice: function (msg) {
            runNotice(msg);
        },
        getMap : function () {
            return map;
        }
    };
}();
