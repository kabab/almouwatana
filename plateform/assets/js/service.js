function request (method, url, params) {
    var req = {
         method: method,
         url: url,
         headers: {
           'Content-Type': 'application/json'
         },
         data: params
    }
    return req;
}

appServices.factory('AuthenticationService', function() {
    var auth = {
        isAuthenticated: false,
        prefix: null
    }
    return auth;
});

appServices.factory('TokenInterceptor', function ($q, $window, $location,$cookieStore, AuthenticationService) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
            }
            return config;
        },
        requestError: function(rejection) {
            return $q.reject(rejection);
        },

        /* Set Authentication.isAuthenticated to true if 200 received */
        response: function (response) {
            if (response != null && response.status == 200 && $window.sessionStorage.token && !AuthenticationService.isAuthenticated) {
                AuthenticationService.isAuthenticated = true;
                if($cookieStore.get('user')){
                    AuthenticationService.hasCookie = true;
                }
            }
            return response || $q.when(response);
        },

        /* Revoke client authentication if 401 is received */
        responseError: function(rejection) {
            if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || AuthenticationService.isAuthenticated)) {
                delete $window.sessionStorage.token;
                AuthenticationService.isAuthenticated = false;
                AuthenticationService.hasCookie = false;
                $cookieStore.remove('user');
                $window.location.href = options.app.base_url + '/#/login';
            }

            return $q.reject(rejection);
        }
    };
});

appServices.factory('UserService', function ($http,$q,$timeout) {
    return {
        signIn: function(user) {
            var def = $q.defer();
            $http.post(options.api.url + '/users/signin', user).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject('error');
            });
            return def.promise;
        },
        logOut: function() {
            return $http.get(options.api.base_url + '/users/logout');
        }
    }
});

appServices.factory('TerritoireService', function ($http,$q,$timeout) {
    return {
        add: function (territoire) {
            var def = $q.defer();
            $http(request('post', options.api.url + '/territoires', territoire)).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data,status, headers, config);
            });
            return def.promise;
        },
        update : function(territoire){
            var def = $q.defer();
            $http(request('put', options.api.url + '/territoires/' + territoire._id, territoire)).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data,status, headers, config);
            });
            return def.promise;
        },
        remove : function(id){
            var def = $q.defer();
            $http(request('delete', options.api.url + '/territoires/' + id, null)).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data,status, headers, config);
            });
            return def.promise;
        },
        list: function (id) {
            var def = $q.defer();
            $http(request('get', options.api.url + '/territoires/', null)).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data,status, headers, config);
            });
            return def.promise;
        },

        addResponsable: function (id, responable_id) {
            var def = $q.defer();
            $http(request('put', options.api.url + '/territoires/' + id + '/' + responable_id, null)).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data,status, headers, config);
            });
            return def.promise;
        },

        removeResponsable: function (id, responable_id) {
            var def = $q.defer();
            $http(request('delete', options.api.url + '/territoires/' + id + '/' + responable_id, null)).success(function (data, status, headers, config) {
                $timeout(function () {
                    def.resolve(data);
                });
            }).error(function (data, status, headers, config) {
                def.reject(data,status, headers, config);
            });
            return def.promise;
        }
    }
}).factory('ProblemeService', function ($http,$q,$timeout) {
    return {
      add: function (probleme) {
          var def = $q.defer();
          $http(request('post', options.api.url + '/problemes', probleme)).success(function (data, status, headers, config) {
              $timeout(function () {
                  def.resolve(data);
              });
          }).error(function (data, status, headers, config) {
              def.reject(data,status, headers, config);
          });
          return def.promise;
      },
      update : function(probleme){
          var def = $q.defer();
          $http(request('put', options.api.url + '/problemes/' + probleme._id, probleme)).success(function (data, status, headers, config) {
              $timeout(function () {
                  def.resolve(data);
              });
          }).error(function (data, status, headers, config) {
              def.reject(data,status, headers, config);
          });
          return def.promise;
      },
      remove : function(id){
          var def = $q.defer();
          $http(request('delete', options.api.url + '/problemes/' + id, null)).success(function (data, status, headers, config) {
              $timeout(function () {
                  def.resolve(data);
              });
          }).error(function (data, status, headers, config) {
              def.reject(data,status, headers, config);
          });
          return def.promise;
      },
      list: function () {
          var def = $q.defer();
          $http(request('get', options.api.url + '/problemes/', null)).success(function (data, status, headers, config) {
              $timeout(function () {
                  def.resolve(data);
              });
          }).error(function (data, status, headers, config) {
              def.reject(data,status, headers, config);
          });
          return def.promise;
      },
      changeEtat: function(id , etat) {
        var def = $q.defer();
        $http(request('get', options.api.url + '/problemes/'+ id + '/' + etat, null)).success(function (data, status, headers, config) {
            $timeout(function () {
                def.resolve(data);
            });
        }).error(function (data, status, headers, config) {
            def.reject(data,status, headers, config);
        });
      return def.promise;
    },
    statistics: function() {
      var def = $q.defer();
      $http(request('get', options.api.url + '/problemes/statistics', null)).success(function (data, status, headers, config) {
          $timeout(function () {
              def.resolve(data);
          });
      }).error(function (data, status, headers, config) {
          def.reject(data,status, headers, config);
      });
      return def.promise;
    },
    locations: function() {
      var def = $q.defer();
      $http(request('get', options.api.url + '/problemes/locations', null)).success(function (data, status, headers, config) {
          $timeout(function () {
              def.resolve(data);
          });
      }).error(function (data, status, headers, config) {
          def.reject(data,status, headers, config);
      });
      return def.promise;
    }
  };
})
.factory('ResponsableService', function($http, $q, $timeout) {
  return {
    add: function (responsable) {
        var def = $q.defer();
        $http(request('post', options.api.url + '/responsables', responsable)).success(function (data, status, headers, config) {
            $timeout(function () {
                def.resolve(data);
            });
        }).error(function (data, status, headers, config) {
            def.reject(data,status, headers, config);
        });
        return def.promise;
    },
    update : function(responsable){
        var def = $q.defer();
        $http(request('put', options.api.url + '/responsables/' + responsable._id, responsable)).success(function (data, status, headers, config) {
            $timeout(function () {
                def.resolve(data);
            });
        }).error(function (data, status, headers, config) {
            def.reject(data,status, headers, config);
        });
        return def.promise;
    },
    remove : function(id){
        var def = $q.defer();
        $http(request('delete', options.api.url + '/responsables/' + id, null)).success(function (data, status, headers, config) {
            $timeout(function () {
                def.resolve(data);
            });
        }).error(function (data, status, headers, config) {
            def.reject(data,status, headers, config);
        });
        return def.promise;
    },
    list: function () {
        var def = $q.defer();
        $http(request('get', options.api.url + '/responsables/', null)).success(function (data, status, headers, config) {
            $timeout(function () {
                def.resolve(data);
            });
        }).error(function (data, status, headers, config) {
            def.reject(data,status, headers, config);
        });
        return def.promise;
    }
  };
});
