appControllers.controller('CtrlDashboard', ['$scope', 'ProblemeService', '$timeout', '$location',
    function CtrlDashboard($scope, ProblemeService, $timeout, $location) {
        $scope.title_navigation = "ALMOUWATANA";
        $scope.labels = [];
        $scope.labels1 = [];
        $scope.series = ['Series A', 'Series B'];

        $scope.data1 = [];
        $scope.data = [[]];


        ProblemeService.statistics().then(function(data) {
          data[0].data[0].forEach(function(item) {
            $scope.data[0].push(item.count);
            $scope.labels.push(item._id.day + " - " + item._id.month);
          });
          data[0].data[1].forEach(function(item) {
            $scope.data1.push(item.count);
            $scope.labels1.push(item._id);
          });
        });

        ProblemeService.locations().then(function(data) {
          $timeout(function() {
            data[0].data.forEach(function(p) {
              var loc = p.loc;
              var marker = new google.maps.Marker({
                  position: new google.maps.LatLng(loc[0], loc[1]),
                  map: map,
                  title:p._id
              });
              google.maps.event.addListener(marker, 'click', function() {
                window.location.href = '#/agent/probleme/'+ p._id;
              });
            })
          }, 1000);
          map.setZoom(12);
        });
    }
]);

appControllers.controller('UserCtrl', ['$scope','$rootScope','$window','$location','$cookieStore','notify','UserService','AuthenticationService',
    function LoginCtrl($scope, $rootScope,$window,$location,$cookieStore,notify,UserService,AuthenticationService) {
    	$scope.user = {};
      $scope.keepSigned = false;
    	$scope.login = function(form) {
        UserService.signIn($scope.user).then(function(data, status, headers, config) {
          console.log(data);
          if(!data[0].error){
              var user = {
                  admin: data[0].type == 'Admin',
                  email: $scope.user.email,
                  token: data[0].data.token,
                  isAuthenticated: true
              };
              AuthenticationService.isAuthenticated = true;
              $window.sessionStorage.token = data[0].data.token;
              $cookieStore.put('user', user);
              $rootScope.currentUser = user;
              if(data[0].type == "Responsable") {
                $location.path("/responsable");
              } else {
                $location.path("/agent");
              }
            } else {
              notify({duration: 2000, message: data[0].msg, classes: 'alert-danger'});
            }
        },function(data, status, headers, config) {
          notify({duration: 2000, message: "Probleme de connexion", classes: 'alert-danger'});
        });

    }
  }
]);
appControllers.controller('ProblemeCtrl', [ 'ProblemeService', '$scope', 'notify', '$timeout', '$routeParams', 'ngTableParams',
  function(ProblemeService, $scope, notify, $timeout, $routeParams, ngTableParams) {
    $scope.problemes = [];

    var fetchProbleme = function() {
      ProblemeService.list().then(function(data) {
        if(!data.error)
          $scope.problemes = data[0].data;
          loadProbleme();
      }, function() {
        notify({duration: 5000, message: "Probleme de connexion", classes: 'alert-danger'});
      });
    };

    var loadProbleme = function() {
      if($routeParams.id) {
        $scope.problemes.forEach(function(p) {
          if(p._id == $routeParams.id) {
            $scope.probleme = p;
          }
        });

        $timeout(function() {
          var loc = $scope.probleme.loc;
          var marker = new google.maps.Marker({
              position: new google.maps.LatLng(loc[0], loc[1]),
              map: map,
              title: 'Position de dommage causé'
          });
          map.setCenter(marker.getPosition());
        }, 1000);
      }
    }

    var socket = io('http://localhost:3001');

    socket.on('news', function (data) {
      if(!$routeParams.id) {
        notify({message: "Probléme ajouté", classes: 'alert-success'});
        $timeout(fetchProbleme, 1000);
      }
    });

    $scope.etatChanged = function(item) {
      ProblemeService.changeEtat(item._id, item.etat).then(function(data) {
        if(!data[0].error) {
          notify({message: "L'état de probleme est changé", classes: 'alert-success'});
        } else {
          notify({message: data[0].error, classes: 'alert-danger'});
        }
      }, function() {
        notify({duration: 2000, message: "Probleme de connexion", classes: 'alert-danger'});
      });
    }

    $scope.update = function() {
      ProblemeService.update($scope.probleme).then(function(data) {
        if(!data[0].error) {
          notify({message: data[0].msg, classes: 'alert-success'});
        } else {
          data.forEach(function(d) {
            notify({message: d.msg, classes: 'alert-success'});
          });
        }
      }, function() {
        notify({duration: 2000, message: "Probleme de connexion", classes: 'alert-danger'});
      });
    }



    $scope.remove = function(i) {
      var id = $scope.problemes[i]._id;
      ProblemeService.remove(id).then(function(data) {
        if(!data[0].error) {
          notify({message: data[0].msg, classes: 'alert-success'});
          $scope.problemes.splice(i, 1);
        } else {
          data.forEach(function(d) {
            notify({message: d.msg, classes: 'alert-success'});
          });
        }
      }, function() {
        notify({duration: 2000, message: "Probleme de connexion", classes: 'alert-danger'});
      });
    }

    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10           // count per page
    }, {
        total: $scope.problemes.length, // length of data
        getData: function ($defer, params) {
            $defer.resolve($scope.problemes.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });

    fetchProbleme();
}]);

appControllers.controller('ModalCtrl', function($scope, $modalInstance) {
  $scope.territoire = {};
  $scope.ok = function () {
   $modalInstance.close($scope.territoire);
  };

  $scope.cancel = function () {
   $modalInstance.dismiss('cancel');
  };
});

appControllers.controller('TerritoireCtrl', ['$modal', '$scope','$rootScope', '$routeParams','$location','$window','TerritoireService', '$timeout', 'notify',
    function ($modal, $scope,$rootScope, $routeParams, $location, $window, TerritoireService, $timeout, notify) {

      $scope.territoires = [];

      $rootScope.selectedTerritoire = null;

      var fetchTerritoires = function() {
        TerritoireService.list().then(function(data) {
          if(data[0].error) {
              notify({message: data[0].msg, classes: 'alert-success'});
          } else {
            $scope.territoires = data[0].data;
          }
        }, function() {
          notify({message: 'Probléme dans la connexion', classes: 'alert-success'});
        });
      };

      $scope.changeColor = function(i, b) {
        if(!$scope.territoires[0].p)  return;
        if(b)
          $scope.territoires[i].p.setOptions({fillOpacity: 0.8});
        else
          $scope.territoires[i].p.setOptions({fillOpacity: 0.2});
      }

      var drawOnMap = function() {
        $scope.territoires.forEach(function(t) {
          var coord = [];
          t.points.forEach(function(p) {
            coord.push(new google.maps.LatLng(p[0], p[1]));
          });
          t.p = new google.maps.Polygon({
            paths: coord,
            editable: true,
            draggable: true,
            fillOpacity: 0.2
          });

          t.p.setMap(map);

          google.maps.event.addListener(t.p, 'mouseup', function(r) {
            var nt = {};
            nt.points = [];
            nt._id = t._id;
            nt.nom = t.nom;
            t.p.getPath().j.forEach(function(point) {
              nt.points.push([point.A, point.F]);
            });
            TerritoireService.update(nt).then(function(data) {
              if(data[0].error) {
                  notify({message: data[0].msg, classes: 'alert-success'});
              }
            }, function() {
              notify({message: 'Probléme dans la connexion', classes: 'alert-success'});
            });
          });
        });
      }

      $timeout(
      function() {
        var drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: google.maps.drawing.OverlayType.POLYGON,
          drawingControl: true,
          drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
              google.maps.drawing.OverlayType.POLYGON,
            ]
          },
          polygonOptions: {
            fillOpacity: 0.2,
            draggable: true,
            editable: true
          }
        });

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function (p) {
          var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'myModalContent.html',
            controller: 'ModalCtrl',
          });
          modalInstance.result.then(function(t) {
            $scope.territoire = {};
            $scope.territoire.points = [];
            $scope.territoire.nom = t.nom || '';

            if($scope.territoire.nom <= 0) {
              notify({message: 'Nom invalide', classes: 'alert-success'});
              p.setMap(null);
              return;
            }
            p.getPath().j.forEach(function(point) {
              $scope.territoire.points.push([point.A, point.F]);
            });
            console.log(JSON.stringify($scope.territoire.points));

            TerritoireService.add($scope.territoire).then(function(data) {
              if(!data[0].error) {
                notify({message: 'Territoire ajouté', classes: 'alert-success'});
                $scope.territoire.p = p;
                $scope.territoires.push($scope.territoire);
              } else {
                notify({message: 'Erreur ' + data[0].msg, classes: 'alert-success'});
                p.setMap(null);
              }
            }, function() {
              notify({message: 'Erreur de connexion', classes: 'alert-success'});
              p.setMap(null);
            });

          }, function() {
            p.setMap(null);
          });
        });
        drawingManager.setMap(map);
        drawOnMap();
      }, 1000);

      $scope.open = function (i) {
        $rootScope.selectedTerritoire = $scope.territoires[i];
        var modalInstance = $modal.open({
          animation: true,
          templateUrl: 'TerritoireModal.html',
          controller: 'TerritoireModalCtrl'
        });
      };
      fetchTerritoires();
}]);

appControllers.controller('TerritoireModalCtrl', ['$scope', '$rootScope', 'TerritoireService', '$modalInstance', 'ResponsableService', 'notify',
  function($scope, $rootScope, TerritoireService, $modalInstance, ResponsableService, notify) {
    $scope.title = $rootScope.selectedTerritoire.nom;
    var fetchSelected = function() {
      $scope.selectedRes = [];
      $scope.responsables.forEach(function(responsable) {
        if($rootScope.selectedTerritoire.responsable.indexOf(responsable._id) >= 0) {
          $scope.selectedRes.push(responsable);
        }
      });
    }

    ResponsableService.list().then(function(data) {
        if(!data[0].error) {
          $scope.responsables = data[0].data;
          fetchSelected();
        } else {
          notify({message: data[0].msg, classes: 'alert-success'});
        }
      }, function() {
        notify({message: 'Probleme de connexion', classes: 'alert-success'});
    });

    $scope.add = function() {
      TerritoireService.addResponsable($rootScope.selectedTerritoire._id, $scope.selected)
      .then(function(data) {
          if(!data[0].error) {
            $scope.selectedTerritoire.responsable.push($scope.selected);
            fetchSelected();
          } else {
            notify({message: data[0].msg, classes: 'alert-success'});
          }
        }, function() {
          notify({message: 'Probleme de connexion', classes: 'alert-success'});
      });
    }

    $scope.remove = function(i) {
      TerritoireService.removeResponsable($rootScope.selectedTerritoire._id, i)
      .then(function(data) {
          if(!data[0].error) {
            var i = $scope.selectedTerritoire.responsable.indexOf(i);
            $scope.selectedTerritoire.responsable.splice(i, 1);
            fetchSelected();
          } else {
            notify({message: data[0].msg, classes: 'alert-success'});
          }
        }, function() {
          notify({message: 'Probleme de connexion', classes: 'alert-success'});
      });
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('cancel');
    }
}]);

appControllers.controller('ResponsableCtrl', ['$scope','$rootScope','$cookieStore','$location','ResponsableService', 'notify', '$routeParams',
  function($scope, $rootScope, $cookieStore, $location, ResponsableService, notify, $routeParams) {
    $scope.responsables = null;
    $scope.r = {};

    ResponsableService.list().then(function(data) {
        if(!data[0].error) {
          $scope.responsables = data[0].data;
          loadResponsable();
        } else {
          notify({message: data[0].msg, classes: 'alert-success'});
        }
      }, function() {
        notify({message: 'Probleme de connexion', classes: 'alert-success'});
    });

    var loadResponsable = function() {
      if($routeParams.id) {
        $scope.formTitle = "Responsable : " + $routeParams.id;
        $scope.title = "Modifier un responsable";
        $scope.buttonLbl = "Enregistrer"
        $scope.responsables.forEach(function(r) {
          if($routeParams.id == r._id) {
            delete r.password;
            $scope.r = r;
          }
        });
      } else {
        $scope.buttonLbl = "Ajouter"
        $scope.formTitle = "Nouveau reponsable";
        $scope.title = "Ajouter un responsable";
      }
    }


    $scope.update = function() {
      ResponsableService.update($scope.r).then(function(data) {
          if(!data[0].error) {
            notify({message: data[0].msg, classes: 'alert-success'});
          } else {
            data.forEach(function(d) {
              notify({message: d.msg, classes: 'alert-success'});
            });
          }
        }, function() {
          notify({message: 'Probleme de connexion', classes: 'alert-success'});
      });
    }

    $scope.add = function() {
      ResponsableService.add($scope.r).then(function(data) {
          if(!data[0].error) {
            notify({message: data[0].msg, classes: 'alert-success'});
            $location.path('/agent/responsable');
          } else {
            data.forEach(function(d) {
              notify({message: d.msg, classes: 'alert-success'});
            });
          }
        }, function() {
          notify({message: 'Probleme de connexion', classes: 'alert-success'});
      });
    }

    $scope.action = function() {
      if($scope.r._id) {
        $scope.update();
      } else {
        $scope.add();
      }
    }

    $scope.remove = function(i) {
      ResponsableService.remove($scope.responsables[i]._id).then(function(data) {
        if(!data[0].error) {
          notify({message: data[0].msg, classes: 'alert-success'});
          $scope.responsables.splice(i, 1);
        } else {
          data.forEach(function(d) {
            notify({message: d.msg, classes: 'alert-success'});
          });
        }
      }, function() {
        notify({message: 'Probleme de connexion', classes: 'alert-success'});
      });
    }

}]);
