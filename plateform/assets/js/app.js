'use strict';

var app = angular.module('app', [
  'ngRoute',
  'appControllers',
  'appServices',
  'ngCookies',
  'chart.js',
  'toggle-switch',
  'ui.bootstrap',
  'cgNotify',
  'ngTable'
]);

var appControllers = angular.module('appControllers', []);
var appServices = angular.module('appServices', []);
var options = {};
options.api = {};
options.app = {};

options.api.url = "http://localhost:3001";
//options.api.url = "http://localhost:8080/checkly/webresources";
options.api.url_local = "http://localhost:8080/checkly/webresources";
options.app.url = "http://localhost/checkly";
options.app.url_local = "http://localhost/checkly/#/";

app.config(['$routeProvider','$locationProvider',
  function($routeProvider,$locationProvider) {
    $routeProvider.
      when('/agent/', {
        controller: 'CtrlDashboard',
        templateUrl: 'parts/agent/dashboard.html',
        access: { requiredAuthentication: true }
      }).
      when('/agent/probleme', {
        controller: 'ProblemeCtrl',
        templateUrl: 'parts/agent/probleme.list.html',
        access: { requiredAuthentication: true }
      }).
      when('/agent/probleme/:id', {
        controller: 'ProblemeCtrl',
        templateUrl: 'parts/agent/probleme.form.html',
        access: { requiredAuthentication: true }
      }).
      when('/responsable/', {
        controller: 'CtrlDashboard',
        templateUrl: 'parts/agent/dashboard.html',
        access: { requiredAuthentication: true }
      }).
      when('/responsable/probleme', {
        controller: 'ProblemeCtrl',
        templateUrl: 'parts/agent/probleme.list.html',
        access: { requiredAuthentication: true }
      }).
      when('/responsable/probleme/:id', {
        controller: 'ProblemeCtrl',
        templateUrl: 'parts/agent/probleme.form.html',
        access: { requiredAuthentication: true }
      }).
      when('/agent/responsable', {
        controller: 'ResponsableCtrl',
        templateUrl: 'parts/agent/responsable.list.html',
        access: { requiredAuthentication: true }
      }).
      when('/agent/responsable/form', {
        controller: 'ResponsableCtrl',
        templateUrl: 'parts/agent/responsable.form.html',
        access: { requiredAuthentication: true }
      }).
      when('/agent/responsable/:id', {
        controller: 'ResponsableCtrl',
        templateUrl: 'parts/agent/responsable.form.html',
        access: { requiredAuthentication: true }
      }).
      when('/agent/territoire', {
        controller: 'TerritoireCtrl',
        templateUrl: 'parts/agent/maps.html',
        access: { requiredAuthentication: true }
      }).
      when('/login', {
        controller: 'UserCtrl',
        templateUrl: 'parts/login.html',
        css: 'assets/css/login-themes.css'
      }).otherwise({
        redirectTo: '/login'
      });
}]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
});

app.run(function ($rootScope,$cookieStore,$window,$location,AuthenticationService) {

  if($cookieStore.get('user') != null){
    $rootScope.currentUser = $cookieStore.get('user');
    AuthenticationService.isAuthenticated = true;
    AuthenticationService.prefix = $rootScope.currentUser.type;
  }

  $rootScope.$on('$viewContentLoaded', function() {
    Checkly.init();
  });

  $rootScope.$on("$routeChangeStart", function (event, nextRoute, currentRoute) {
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
            && !AuthenticationService.isAuthenticated) {
            $location.path("/login");
        }
        if($location.path() == '/login' && AuthenticationService.isAuthenticated){
          //$window.location.href = options.app.url_local + AuthenticationService.prefix;
          $window.location.href = options.app.url + AuthenticationService.prefix;
        }
  });
  $rootScope.$on('$routeChangeSuccess', function (event, nextRoute, currentRoute) {

  });

});

app.directive('head', ['$rootScope','$compile',
    function($rootScope, $compile){
        return {
            restrict: 'E',
            link: function(scope, elem){
                var html = '<link rel="stylesheet" ng-repeat="(routeCtrl, cssUrl) in routeStyles" ng-href="{{cssUrl}}" />';
                elem.append($compile(html)(scope));
                scope.routeStyles = {};
                $rootScope.$on('$routeChangeStart', function (e, next, current) {
                    if(current && current.$$route && current.$$route.css){
                        if(!angular.isArray(current.$$route.css)){
                            current.$$route.css = [current.$$route.css];
                        }
                        angular.forEach(current.$$route.css, function(sheet){
                            delete scope.routeStyles[sheet];
                        });
                    }
                    if(next && next.$$route && next.$$route.css){
                        if(!angular.isArray(next.$$route.css)){
                            next.$$route.css = [next.$$route.css];
                        }
                        angular.forEach(next.$$route.css, function(sheet){
                            scope.routeStyles[sheet] = sheet;
                        });
                    }
                });
            }
        };
    }
]);
